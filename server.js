'use strict';

// Load .env
require('dotenv').config()

const express                   = require('express');

const { log, error }            = require('./src/log');
const { HandlerGenerator }      = require('./src/handlers');
const { CrawlRegister }         = require('./src/modelsModified');             
const { crawlRoutine }          = require('./src/routines');
const constants                 = require('./src/constants');
const {
  setIntervalAsync,
  setIntervalAndRunAsync
}                               = require('./src/utilities');
const {machineID:uuid}          = require('./src/machineID');
const {sendMetrics}             = require('./src/cloudWatch');


// Build app
const app                       = express();
const port                      = process.env.port || 3000;

const handlers                  = new HandlerGenerator();

app.get( '/',       handlers.index);
app.get( '/health', handlers.index);
app.post('/crawl',  handlers.crawl);

// Listen
app.listen(port, () => {
  log(`Listening on port ${port}!`);
});


// Set up the intervals
(async ()=>{

  // Commence Routines
  log(`Commencing Rountines for UUID: ${uuid}`);

  // Register & Clean
  setIntervalAndRunAsync(async ()=>{
    try{
      await CrawlRegister.register();
    }catch(err){
      error(err);
    }
  },constants.REGISTER_INTERVAL);

  // Distribute Domains
  setIntervalAndRunAsync(async ()=>{
    try{
      await CrawlRegister.distributeDomains(); 
    }catch(err){
      error(err)
    }
  },constants.DISTRIBUTION_INTERVAL);

  //Commence Crawling
  setIntervalAsync(async ()=>{
    try{
      await crawlRoutine();
    }catch(err){
      error(err);
    }
  },constants.CRAWL_INTERVAL);

  // Send Cloud Metrics
  setIntervalAsync(sendMetrics,constants.SEND_METRICS_INTERVAL)

})();


/*
(async ()=>{
  setTimeout(async ()=>{
    let url = "https://www.danmurphys.com.au/product/DM_41788/lagavulin-16-year-old-islay-single-malt-scotch-whisky-700ml"
    //let url = "https://www.google.com"
    let payload = await crawl(url);
    console.log(payload);
  },500)
})();
*/

/*
(async ()=>{
  let url = "http://google.com"
  let crawlObject = await Crawl.findOne({url}) || new Crawl({url});
  await crawlObject.save();
  await crawlObject.crawl();
})();
*/
