module.exports = {
  apps : [
    {
      name: "traibe-crawler",
      script: "npm start",
      watch: true,
      env: {
          "PORT": 5000,
          "NODE_ENV": "development",
          "AWS_REGION": process.env.AWS_REGION,
          "AWS_SES_REGION": process.env.AWS_SES_REGION,
          "AWS_ACCESS_KEY_ID": process.env.AWS_ACCESS_KEY_ID,
          "AWS_SECRET_ACCESS_KEY": process.env.AWS_SECRET_ACCESS_KEY,
          "MONGO_DATABASE": "crawler_development"
      },
      env_production: {
          "PORT": 3000,
          "NODE_ENV": "production",
          "AWS_REGION": process.env.AWS_REGION,
          "AWS_SES_REGION": process.env.AWS_SES_REGION,
          "AWS_ACCESS_KEY_ID": process.env.AWS_ACCESS_KEY_ID,
          "AWS_SECRET_ACCESS_KEY": process.env.AWS_SECRET_ACCESS_KEY,
          "MONGO_DATABASE": "crawler_production"
      }
    }
  ],

  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};

