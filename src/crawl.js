'use strict';

const http              = require("http");
const puppeteer         = require('puppeteer');
const ua                = require('random-useragent'); // Random UserAgent
const fs                = require('fs');
const path              = require('path')
const dns               = require('dns');
const uuidv5            = require('uuid/v5');
const {
    validateURL,
    readFile,
    sleepAsync
}                       = require('./utilities');
const {
    log,
    error,
    warn
}                       = require('./log');
const { saveToBucket }  = require('./store');
const { CaptchaError}   = require('./errors');
const constants         = require('./constants');


const bucket_name       = process.env.BUCKET_NAME;
if(!bucket_name)
    throw new Error("BUCKET_NAME is required as a environmental variable");


const crawl = async (url) => {

    // Record the start time
    const timeStart = Date.now();

    // Variables
    var success       = false, 
        page          = null,
        browser       = null,
        pageResponse  = undefined,
        statusCode    = 400;

    // Default payload
    var payload      = defaultResponse(statusCode);

    tryOuter : try { 

        log(`Crawling: ${url}`);

        // Check the internet
        await checkInternetConnection();

        // Decode
        url = decodeURIComponent(url);

        // Validate
        validateURL(url, "Target URL");

        // Get Proxy Credentials
        let {proxy_disable, proxy_address, proxy_username, proxy_password} = proxyCredentials();

        // Default args
        let args = puppeteer.defaultArgs();

        // Random window size
        if(constants.RANDOM_WINDOW){
            let {width, height} = randomWindow();
            args = args.concat([
                `--window-size=${width},${height}`
            ]);
            log(`Setting Window to: ${width}x${height}`);
        }

        // Proxy Enable and Proxy Address
        if(!proxy_disable && proxy_address)
            args = args.concat([
                `--proxy-server=${proxy_address}`,
                '--disable-gpu', 
                '--no-sandbox', 
                '--single-process', 
                //'--disable-web-security', 
                '--disable-dev-profile'
            ])

        // Launch headless chrome
        browser = await puppeteer.launch({
            args            : args,
            defaultViewport : puppeteer.defaultViewport,
            executablePath  : await puppeteer.executablePath(),
            headless        : constants.HEADLESS
        });

        // NEW Page
        page = await browser.newPage();

        // Page Events
        handlePageEvents(page);

        //Clear cookies
        await clearCookies(page, url);

        // The random useragent
        await randomUserAgent(page);

        // Remove Webdriver
        await removeWebdriver(page);

        // Authenticate, if proxy has been enabled
        if(!proxy_disable) 
            await authenticateProxy(page, proxy_username, proxy_password);

        // Ping the proxy
        await ping(proxy_address);

        let now = Date.now();
        try {

            // Load page
            pageResponse = 
            await page.goto(url, { timeout : constants.CRAWL_TIMEOUT, waitUntil : 'networkidle2'});

            // Dismiss Modals
            await dismisModals(page);

            // Wait for Images
            await page.waitForFunction(imagesHaveLoaded, { timeout : constants.CRAWL_TIMEOUT });

        }catch(err){
            if(err instanceof puppeteer.errors.TimeoutError)
                error(`Timed out after ${Date.now() - now}ms (Max. ${2 * constants.CRAWL_TIMEOUT}ms), ${url}`)
            throw err;
        }

        // If no page response, break from subsequent
        if(!pageResponse){
            log("Page response is undefined");
            break tryOuter;
        }

        log("Page retrieved, processing");

        // Status Code from page response
        statusCode  = pageResponse.status();
        payload     = {...payload, statusCode, body: { statusCode }}

        // Success, save html and screenshots
        if(statusCode == 200){

            // Check for recaptcha
            const html = await page.content();
    
            // Save Source
            payload.body.source = await saveSource(page, bucket_name)
    
            // Screenshot
            try{
                payload.body.screenshots = await saveScreen(page, bucket_name);
            }catch(err){
                warn(`Problem saving screenshot: ${err.message}`);
            }
    
            if(html.match(/\sCAPTCHA\s/))
                throw new CaptchaError("HTML contains captcha-type text");
        }

        // Success !!
        success = true;

    // Error Handling
    }catch(err){

        // puppeteer Timeout
        if (err instanceof puppeteer.errors.TimeoutError) {
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/408
            statusCode = 408;

        // Captcha
        } else if (err instanceof CaptchaError) {
            statusCode = 422
        
        // Everything Else
        } else {
            statusCode = 400;
        }
    
        // Done, Modify default response
        error(`Status: ${statusCode}x, ${err.message}`);
    
        //Build payload
        payload = {
            ...payload,
            statusCode: statusCode,
            body: {
                statusCode : statusCode,
                message    : err.toString()
            }
        };

    // Cleanup
    }finally{
        unHandlePageEvents(page);
        if (page !== null)      await page.close();
        if (browser !== null)   await browser.close();
    }

    // Report Time Elapsed
    const timeFinish = Date.now();
    log(`Duration: ${timeFinish - timeStart}ms`);

    // Done
    return payload;
};

/* 
    Ping the luminati proxy
*/
const ping = async (proxy_address, path='/ping') => {
    try{
        let url = new URL(proxy_address);
        let {port, hostname } = url;
        http.get({host:hostname, port, path},res => {
            log(`Proxy Ping Status: ${res.statusCode}`)
            let body = "";

            res.on("data", (chunk) => { 
                body += chunk; 
            });

            res.on("end", () => {
                try {
                    let json = JSON.parse(body);
                    log(`Proxy Ping Server: ${JSON.stringify(json)}`);
                } catch (err) {
                    error(err.message);
                };
            });
        }).on("error",(err)=>{
            error(err.message);
        });
    }catch(err){
        error(err.message);
    }
}

const respondOrNavigate = async (page, timeout) => {
    try{
        let responseEventOccurred   = false;
        const responseHandler       = (event) => (responseEventOccurred = true);
        const responseWatcher       = new Promise(function(resolve, reject){
            setTimeout(() => {
                if (!responseEventOccurred) {
                    resolve(); 
                } else {
                    setTimeout(resolve,timeout - 500);
                }
                page.removeListener('response', responseHandler);
            }, 500);
        });
        page.on('response', responseHandler);
        return Promise.race([ responseWatcher, page.waitForNavigation({timeout}) ]);
    }catch(err){
        error(err.message);
    }
}

const dismisModals = async (page) => {
    // Click at origin to get rid of pesky modals
    try{
        // await sleepAsync(10000);
        // await page.mouse.click(0,0);
        // await sleepAsync(5000);
    }catch(err){
        error(err.message)
    }
}

/*
    Check internet connection
*/
const checkInternetConnection = async () => {
    log("Checking for internet connection");
    return new Promise((resolve, reject)=>{
        dns.resolve('www.google.com', (err)=>{
            if(err){ 
                error("No internet connection"); 
                return reject(err); 
            }
            log("Internet Connected OK");
            return resolve();
        }); 
    })
}

/*
    Authenticate the proxy
*/
const authenticateProxy = async(page, username, password) => {
    log('Authenticating Proxy');
    try{
        await page.authenticate({username, password});
        return Promise.resolve();
    }catch(err){
        return Promise.reject(err);
    }
}

/*
    Remove the webdriver traces
    See: https://stackoverflow.com/a/56336300/1834057
*/
const removeWebdriver = async (page) => {
    try{
        await page.evaluateOnNewDocument(() => { 
            delete navigator.__proto__.webdriver; 
        });
        return Promise.resolve();
    }catch(err){
        return Promise.reject(err);
    }
}

/*
    Function to apply the random user agent
*/
const randomUserAgent = async (page) => {
    try{
        let agent;
        agent = ua.getRandom();
        await page.setUserAgent(agent);
        agent = await page.evaluate(() => navigator.userAgent)
        log(`Current User Agent: ${agent}`);
        return Promise.resolve();
    }catch(err){
        return Promise.reject(err);
    }
}

/*
    Get current cookies from the page for certain URL, and remove them
*/
const clearCookies = async (page, url) => {
    try{
        const cookies = 
        await page.cookies(url);
        await page.deleteCookie(...cookies);
    }catch(err){
        // Not critical
    }
    return Promise.resolve();
}

const defaultResponse = (defaultStatusCode) => {
    return {
        statusCode: defaultStatusCode,
        body: {}
    }
}

const proxyCredentials = () => {
    /*
        Proxy details:
        Default use proxy unless explicitly say no.
        When proxy is required, and no proxy_username or proxy_password,
        then error will be thrown.
    */
    let proxy_disable  = process.env.PROXY_DISABLE == 'true',
        proxy_address  = process.env.PROXY_ADDRESS,
        proxy_username = process.env.PROXY_USERNAME,
        proxy_password = process.env.PROXY_PASSWORD;

    // Enabled
    if(!proxy_disable){
        if(!proxy_username)
            throw new Error("'PROXY_USERNAME' environmental variable is required for authentication.");
        if(!proxy_password)
            throw new Error("'PROXY_PASSWORD' environmental variable is required for authentication.");
        if(!proxy_address)
            throw new Error("'PROXY_ADDRESS' environmental variable is required.");

        // Ensure URL is ok
        validateURL(proxy_address,'Proxy Address');
    }

    // Return the set of credentials
    return {
        proxy_disable, 
        proxy_address, 
        proxy_username, 
        proxy_password
    }
}

const randomWindow = () => {
    const window_sizes = [
        {width:1920,height:1080},
        {width:1600,height:900},
        {width:1366,height:768},
        {width:1280,height:720},
    ]

    // Random size
    let random_ix = Math.floor(Math.random() * window_sizes.length);
    let {width,height} = window_sizes[random_ix];

    // Widow size entropy
    if(Math.random() < 0.5){
        let shift = 50;
        width   = width   + Math.floor((Math.random() * 2 * shift) - shift)
        height  = height  + Math.floor((Math.random() * 2 * shift) - shift)
    }
    return {
        width,
        height
    }
}

function imagesHaveLoaded() { 
    return Array.from(document.images).every((i) => i.complete); 
}

const getBytes = (string) => {
    return Buffer.byteLength(string, 'utf8')
}

const saveSource = async function(page, bucket){
    try{
        const url = page.url().split('?')[0];
        if(!url) 
            throw new Error("Problem calling url.");
        const uuidnm = uuidv5(url, uuidv5.URL);

        // Build the key
        const ext = path.extname(url)
        const key = `${constants.FOLDER_SOURCES}/${uuidnm}.html`;
        log(`File Name: ${key}`);

        // Get the html
        const html = await page.content();
        log(`File Len.: ${html.length} char`)
        log(`File Size: ${(getBytes(html)/1024.0).toFixed(1)} KiB`);

        // Write to bucket
        const data = await saveToBucket(bucket, key, html);

        // Resolve
        return Promise.resolve(data)
    }catch(err){
        //Reject
        return Promise.reject(err);
    }
}

/*
  Save screenshot for common screen sizes
*/
async function saveScreen(page, bucket){

    // Permutations of screen dimensions
    const dims = [
        {w:1920, h:1080},
        /*
            {w:1400, h:900},
            {w:1366, h:768},
            {w:1280, h:800},
            {w:360,  h:640}
        */
    ]
    
    // Permutations of full screens
    const fullScreen = [true, false]

    // Empty result array
    var result = []

    // Now loop over the combos, adding to the result array
    for(let dim of dims){
        for(let full of fullScreen){
            try{
                let payload = await saveScreenWH(page, dim.w, dim.h, full, bucket);
                result.push(payload);
            }catch(err){
                // Nothing
            }
        }
    }

    // Done
    return Promise.resolve(result);
}

/*
  Save screenshot for current page, provided width(>=1), height(>=1) and fullscreen (true/false)
*/
async function saveScreenWH(page, width, height, fullPage, bucket){

    let fileName = undefined;
    
    try{
        const url = page.url();
        if(!url) 
            throw("Problem calling url.");
        if(width < 1 || height < 1) 
            throw('Width and height must be positive non-zero');

        fullPage = fullPage ? true : false

        // Build the key: <UID>_screenshot_<SUFFIX><FSC>.png
        const uid = uuidv5(url, uuidv5.URL);
        const sfx = `${width}x${height}`;
        const fsc = `${fullPage ? '_fullscreen' : ''}`;
        const key = `${constants.FOLDER_SCREENSHOTS}/${uid}_screenshot_${sfx}${fsc}.png`;
        log(`Screenshot: [${sfx}], ${fullPage ? 'FULLSCREEN' : 'STANDARD'}`);
        log(`Screenshot FileName: ${key}`);

        // Temporary file, can write to /tmp folder
        fileName = `/tmp/${path.parse(key).base}`

        //Set the viewport
        await page.setViewport({width, height});

        // Take the screenshot
        await page.screenshot({path:fileName,fullPage}); 

        // Read the image buffer
        const buffer = await readFile(fileName);

        // Save to bucket
        const source = await saveToBucket(bucket, key, buffer);

        // Resolve
        return Promise.resolve({width, height, fullPage, source});

    }catch(err){

        //Problem
        return Promise.reject(err);

    }finally{

        // Remove temp files
        if(fileName) 
            fs.unlinkSync(fileName);
    }
}

// Page Events
const handlePageEvents = (page) => {
    if(!Boolean(page)) return;
    try{
        log('Adding Page Listeners');
        
        // Handle Error
        page.on('error', (msg) => {
            error(`Page Error: ${msg}`)
            throw new Error(msg);
        });

        // Handle dialog
        page.on('dialog', async (dialog) => {
            try{
                log(`Page Dialogue Message: ${dialog.message()}`);
                await dialog.dismiss();
            }catch(err){
                error(err.message);
            }
        });

        // Handle Load
        page.on('load', async () => {
            log(`Page Loaded: ${page.url()}`);
        });
    }catch(err){
        error(err.message);
    }
}

const unHandlePageEvents = (page) => {
    if(!Boolean(page)) return;
    try{
        log('Removing Page Listeners');
        page.removeAllListeners('error');
        page.removeAllListeners('dialog');
        page.removeAllListeners('load');
        page.removeAllListeners('response'); // added in main code
    }catch(err){
        error(err.message);
    }
}

module.exports = {crawl}
