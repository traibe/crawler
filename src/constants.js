// Registration Interval
const CRAWL_REQUEST_SEND_URLS_INTERVAL = 60 * 60 * 1000; 
const CRAWL_INTERVAL        =   5 * 1000;
const CRAWL_TIMEOUT         =   60 * 1000;
const REGISTER_INTERVAL     =   10 * 1000;
const DISTRIBUTION_INTERVAL =   60 * 1000;
const SEND_METRICS_INTERVAL =   5 * 1000;
const CRAWL_BATCH_SIZE      =   5;
const HEADLESS              =   false;
const RANDOM_WINDOW         =   true;
const SNS_TOPIC_RESULT      =   'ms-crawl-result';
const SQS_TOPIC_REQUEST     =   'ms-crawl-request-sub-ms-crawl';
const FOLDER_SOURCES        =   'sources'
const FOLDER_SCREENSHOTS    =   'screenshots'

module.exports ={
    CRAWL_REQUEST_SEND_URLS_INTERVAL,
    CRAWL_INTERVAL,
    CRAWL_TIMEOUT,
    REGISTER_INTERVAL,
    DISTRIBUTION_INTERVAL,
    CRAWL_BATCH_SIZE,
    SNS_TOPIC_RESULT,
    SQS_TOPIC_REQUEST,
    FOLDER_SOURCES,
    FOLDER_SCREENSHOTS,
    HEADLESS,
    RANDOM_WINDOW,
    SEND_METRICS_INTERVAL
}