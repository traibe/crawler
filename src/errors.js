class CaptchaError extends Error {
    constructor(args){
      super(args);
      this.name = "CaptchaError"
    }
}

module.exports = {
    CaptchaError
}