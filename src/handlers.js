const crawl                 = require('./crawl');

class HandlerGenerator {

    async crawl(req, res){
        let url                 = req.query.url || (req.body || {}).url
        let result              = await crawl(url);
        let {statusCode, body}  = result;
        res.status(statusCode).json(body);
    }

    async index(req, res){
        let statusCode = 200;
        res.status(statusCode).json({
            statusCode  : statusCode,
            success     : true,
            message     : 'OK',
            timestamp   : new Date().toLocaleString("en-US")
        });
    }
}

module.exports = {
    HandlerGenerator
}