"use strict";

const moment    = require('moment');
const util      = require('util');
const Console   = require('console').Console;

// const now = () => new Date().toLocaleString("en-US");
const now = () => moment().format('D MMM HH:mm:ss');

/*
    Extend Console
    see: https://stackoverflow.com/a/59630525/1834057
*/
class Logger extends Console {
    constructor(stdout, stderr, ...otherArgs) {
        super(stdout, stderr, ...otherArgs);
    }

    __timestamp(){
        return `[${now()}]:`;
    }

    log(...args) {
        super.log(this.__timestamp(), util.format(...args));
    }

    warn(...args){
        super.warn(this.__timestamp(), util.format(...args));
    }

    error(...args) {
        super.error(this.__timestamp(), util.format(...args));
    }
}

module.exports = (function(){
    const logger = new Logger(process.stdout, process.stderr);
    return {
        logger,
        now,
        warn    : logger.warn,
        log     : logger.log,
        error   : logger.error
    }
}());