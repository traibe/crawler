const util                  = require('util');
const moment                = require('moment');
const path                  = require('path');

const {crawl}               = require('../crawl');
const {copyToNewKey}        = require('../store');
const {log, error}          = require('../log');
const { getRandom }         = require('../utilities');
const { publishResult }     = require('../publishResult');
const {db,mongoose}         = require('../database');
const {CrawlArchive}        = require('./crawlArchive');

const {notifications}       = require('@traibe/common/pubsub/crawl');                
const { Crawl : CrawlOrig}  = require('@traibe/models/models/crawl')(mongoose, db.connection);

// New Schema
let CrawlSchema             = CrawlOrig.schema;

CrawlSchema.pre('save',function(next){
    log(`Saving Crawl: '${this._id}'`);
    next();
});

CrawlSchema.methods.archive = async function(bucket, key, debug=false){
    let self = this;
    return new Promise(async (resolve, reject) => {
        try{

            // Determine the crawl date
            let {url, statusCode, crawlDate} = self;

            // Build human readable suffix
            let suffix      = moment(crawlDate).format('YYYYMMDD_HHmmss-SS');
            let extension   = path.extname(key)

            // Construct the new CrawlArchive object
            let obj         = new CrawlArchive({_crawlID : self._id, crawlDate});
            
            // Build the newKey and Make a copy to the archive
            let newKey      = `archive/sources/${self._id}/${obj._id}_${suffix}${extension || '.html'}`

            console.log(bucket      || "No Bucket");
            console.log(key         || "No Key")
            console.log(newKey      || "No New Key")


            let data        = await copyToNewKey(bucket, key, newKey); 

            // Reporting
            if(debug) 
                log(util.inspect(data, {showHidden: false, depth: null}))

            // Now set the location, after the copy has been made
            obj.source  = data.Location;

            // Save and resolve
            obj.save().then(async (x) => {
                await publishResult(notifications.CRAWL_COMPLETE_ARCHIVE_SUCCESS, {statusCode, url, source : data});
                return resolve(x)
            });
        }catch(err){
            try{
                await publishResult(notifications.CRAWL_COMPLETE_ARCHIVE_FAIL, {statusCode, url, error : err});
            }catch(e){
                //
            }
            reject(err);
        }
    })
}

CrawlSchema.methods.crawl = async function(){
    let self    = this;
    let {url}   = self;
    return new Promise(async (resolve, reject) => {
        try{
            // Publish Result -- Commenced
            await publishResult(notifications.CRAWL_COMMENCE,{url});

            let payload = await crawl(url);
            let now     = new Date();
            let {statusCode,body:{source}} = payload;
            let d, m, k, next, archive = false;

            self.crawlDate = new Date(now.getTime());

            // Update Statuds code
            self.statusCode = statusCode;

            //2xx, 3xx success
            if(statusCode >= 200 && statusCode < 400){ 
                self.RC2XX += statusCode <  300 ? 1 : 0; // 200 -> 299
                self.RC3XX += statusCode >= 300 ? 1 : 0; // 300 -> 399
                self.totalSuccesses = self.RC2XX + self.RC3XX;
                self.failures = 0;
                archive = true;
                d = 43200
                m = getRandom(0.75, 1.25);

            //4xx, 5xx failure
            }else if(statusCode >= 400 && statusCode < 600){ 
                self.RC4XX += statusCode <  500 ? 1 : 0; // 400 -> 499
                self.RC500 += statusCode >= 500 ? 1 : 0; // 500 -> 599
                self.totalFailures = self.RC4XX + self.RC5XX;
                self.failures += 1;
                k = 0.9;
                d = 60;
                m = Math.pow(2.0, k * self.failures);
            }else{
                return reject(`Unrecognised StatusCode: ${statusCode}`);
            }
            
            // Set the crawlDate and next crawlDate
            next = new Date(now.getTime());
            next.setSeconds(next.getSeconds() + Math.round(d * m));
            self.crawlNext = next;

            // Publish Crawl Result
            await publishResult(notifications.CRAWL_COMPLETE, { statusCode, url, source, crawlDate : self.crawlDate, crawlNext : self.crawlNext});

            // Mark as Not Processing
            self.processing = false;

            // Save
            await self.save(); // UPDATE RESULTS

            // Now Archive
            if(archive){
                let { _id } = self;
                let { Bucket, Key } = source;
                let obj = self.archive(Bucket, Key);
                payload.source = obj.source;
            }else{
                await publishResult(notifications.CRAWL_COMPLETE_ARCHIVE_ABORT, { statusCode, url });
            }

            // Done
            resolve(payload);

        }catch(err){
            try{
                if(self.processing){
                    self.processing = false;
                    await self.save();
                }
                await publishResult(notifications.CRAWL_ERROR, {url, error:err});
            }catch(e){
                //
            }

            error(err);
            reject(err);

        }
    })
};

// Update to reflect the new changes to the schema
const Crawl = CrawlOrig.compile(CrawlOrig.modelName, CrawlSchema, CrawlOrig.collection.name, CrawlOrig.db, mongoose);

// Export the models
module.exports = {
    Crawl
}