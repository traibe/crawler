const {log, error}          = require('../log');
const {
    chunk,
    arraysContainSameElements,
}                           = require('../utilities');
const {machineID : uuid}    = require('../machineID');
const constants             = require('../constants');             
const {db,mongoose}         = require('../database');

const {Crawl}               = require('./crawl.js');

const {
    CrawlRegister : CrawlRegisterOrig
} = require('@traibe/models/models/crawl')(mongoose, db.connection);

const isMasterCrawler = async function(){
    let crawlers = await CrawlRegister.find({}).sort({createdAt:'ascending'});
    return (crawlers.length > 0 && crawlers[0].uuid === uuid);
}

let CrawlRegisterSchema = CrawlRegisterOrig.schema;

/*
    Clean Crawler Registration Objects that have lapsed
*/
CrawlRegisterSchema.statics.clean = async function(){
    return new Promise((resolve)=>{
        let threshold = new Date(Date.now() - (2 * constants.REGISTER_INTERVAL));
        CrawlRegister.deleteMany({updatedAt:{$lt:threshold}},(err,obj)=>{
            if(err){
                error(err);
                return resolve(0);
            }
            let {deletedCount} = obj;
            if(deletedCount > 0)
                log(`Removed ${deletedCount}x Expired Crawlers`);
            resolve(deletedCount);
        });
    })
}

/*
    Register crawler
*/
CrawlRegisterSchema.statics.register = async function(){

    const Model = db.connection.model(CrawlRegisterOrig.modelName);

    return new Promise(async (resolve, reject) => {
        try{
            // Remove Expired Crawlers
            let removedCrawlers = await CrawlRegister.clean();

            // Determine if redistribution should be forced
            let forceRedistribution = removedCrawlers > 0;

            // Domain redistribution flag
            let redistribute        = false;
            let registerObject      = undefined;

            try{
                // Try to get the register object from the current uuid
                registerObject      = await CrawlRegister.findOne({uuid});
                
                // Register object doesn't exist, create it. 
                // When item is added, then flag redistribution of the domains
                if(!registerObject){
                    log(`Registering Machine UUID: ${uuid}`); 
                    registerObject  = await new Model({uuid}) 
                    redistribute    = true;
                    
                // Force update of the updatedAt timestamp
                }else{
                    registerObject.markModified('uuid');
                }

                //Save the registration object
                registerObject = await registerObject.save();
            
            }catch(err){
                error("Problem Saving Register",err);
            }

            // Redistribute domains across the various objects
            if(redistribute || forceRedistribution) 
                await CrawlRegister.distributeDomains(forceRedistribution);

            // Done
            resolve(registerObject);
        }catch(err){
            error("Problem Registering", err);
            reject(err);
        }
    })  
}

CrawlRegisterSchema.statics.distributeDomains = async function(force=!false){
    try {

        let crawlers    = await CrawlRegister.find({}).sort({createdAt:'ascending'})
        let nCrawler    = crawlers.length;

        // If crawlers exists, and current crawler is master
        if(nCrawler > 0 && (force || await isMasterCrawler())){ 
            
            // Sort by descending aggregation
            let domains = (await Crawl.aggregate([
                    {
                        $match:{
                            crawlNext: {
                                $lte: new Date()
                            }
                        }
                    },
                    {
                        $group: {
                            _id: "$domain",
                            count: { 
                                $sum: 1 
                            } 
                        }
                    },
                    {
                        $sort: {
                            count: -1
                        }
                    }
                ]))
                .filter(x => x.count > 0)
                .map(x => x._id);
            
            let chunked = chunk(domains, nCrawler);

            // Mark as being in transient state if the domains list will change
            let prom1 = crawlers.map((item, index) => {
                let arraysDiffer = !arraysContainSameElements(item.domains,chunked[index])
                if(arraysDiffer){
                    item.ready  = false;
                    return item.save();
                }else{
                    return Promise.resolve();
                }
            })
            await Promise.all(prom1);

            // Assign the new domains, and mark as ready
            let prom2 = crawlers.map((item, index) => {
                let arr         = chunked[index];
                item.ready      = true;
                item.domains    = arr;
                return item.save();
            })
            await Promise.all(prom2);
        }
        return Promise.resolve(crawlers);
    }catch(err){
        return Promise.reject(err);
    }
}

// Update to reflect the new changes to the schema
const CrawlRegister   = CrawlRegisterOrig.compile(CrawlRegisterOrig.modelName, CrawlRegisterSchema, CrawlRegisterOrig.collection.name, CrawlRegisterOrig.db, mongoose);

// Export the models
module.exports      = {
    isMasterCrawler,
    CrawlRegister
}