const {log }                = require('../log');           
const {db,mongoose}         = require('../database');

const {
    CrawlArchive : CrawlArchiveOrig,
} = require('@traibe/models/models/crawl')(mongoose, db.connection);

// CRAWL ARCHIVE
let CrawlArchiveSchema = CrawlArchiveOrig.schema;

CrawlArchiveSchema.pre('save', function(next){
    log(`Saving CrawlArchive: '${this._id}' for Crawl: '${this._crawlID}'`);
    log(`CrawlArchive Source: '${this.source}'`);
    next();
});

// Update to reflect the new changes to the schema
const CrawlArchive = CrawlArchiveOrig.compile(CrawlArchiveOrig.modelName, CrawlArchiveSchema, CrawlArchiveOrig.collection.name, CrawlArchiveOrig.db, mongoose);

// Export the models
module.exports      = {
    CrawlArchive
}


