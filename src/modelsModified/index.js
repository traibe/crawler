const {
    CrawlRegister,
    isMasterCrawler
}   = require('./crawlRegister');
const {CrawlArchive}    = require('./crawlArchive');
const {Crawl}           = require('./crawl');

module.exports = {
    Crawl,
    CrawlArchive,
    CrawlRegister,
    isMasterCrawler
}

/*
// Retrieve the number of items to crawl
schedule = await Crawl
.find({
    domain        : {$in:domains},
    crawlNext     : {$lt:now},
    processing    : {$ne:true} // false or undefined
})
.sort({crawlNext:'ascending'})
.limit(constants.CRAWL_BATCH_SIZE * domains.length);

// Get number of items
numberToCrawl = schedule.length;
if(numberToCrawl <= 0){
log(`Crawl '${uuid}', Domain/s [${domainsJoined}], Up to Date`);
return resolve(0);
}

// Mark as processing
for(let i = 0; i < schedule.length; i++){
try{
    schedule[i].processing = true;
    schedule[i] = await schedule[i].save();
}catch(err){
    error(err);
}
}

// Count the successes
let success = 0;

// If items to crawl, process each one ... 
log(`Crawl '${uuid}', Domain/s [${domainsJoined}], Crawling ${numberToCrawl}x URLS`);

let byGroup = domains.map(d => schedule.filter(s => s.domain === d));

for(let i = 0; i < schedule.length; i++){
try{
    await schedule[i].crawl();

    // Now random sleep if next domain is same
    if(i < schedule.length - 1 && schedule[i].domain === schedule[i+1].domain){
        let rnd = getRandom(10, 60) * 1000;
        log(`Crawl '${uuid}', Domain/s ${domainsJoined}, sleeping for ${rnd}ms`);
        await sleepAsync(rnd);
    }
    success++;
}catch(err){
    error(err);
}
}
return resolve(success);
*/
