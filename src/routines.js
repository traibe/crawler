

const os                    = require('os');
const {log, error}          = require('./log');
const {
    getRandom, 
    chunk,
    validateURL,
    sleepAsync
}                           = require('./utilities');
const {machineID:uuid}      = require('./machineID');
const constants             = require('./constants');
const { publishResult }     = require('./publishResult');

const {listenToQueue}       = require('@traibe/sqs');
const {notifications}       = require('@traibe/common/pubsub/crawl');                
const { mongoose }          = require('@traibe/models');

const ObjectId              = mongoose.Types.ObjectId;

(()=>{
    log("Available Crawler PUB/SUB Notifications:", Object.keys(notifications));
})();


const {
    Crawl,
    CrawlRegister,
    isMasterCrawler
}                           = require('./modelsModified');

// Local function to get the domain hash for the current crawler
const regHash = async () => {
    try{
        let reg = await CrawlRegister.findOne({uuid});
        if(reg) return reg.domainsHash;
    }catch(err){
        error(err);
    }
    return undefined;
};

// Crawl a single domain
const crawlDomains = (domains) => {
    domains = Array.isArray(domains) ? domains : [domains];
    return new Promise(async (resolve)=>{

        // No domains to process
        if(domains.length <= 0)
            return resolve(0);
        
        let now, processing;
        let domainsJoined = domains.join(', ');
        try {

            // Get the domains hash baseline value. 
            // If this doesn't change, then the crawl can continue.
            // If it changes, then this will abort.
            let hashOriginal = await regHash();

            // Get the current timestamp
            now = Date.now();

            // Update time-orphaned crawl objects
            let minutesX    = 5;
            let nowMinusX   = new Date(now - (minutesX * 60 * 1000)); // X minutes in the past
            await Crawl.updateMany(
                { domain : {$in: domains}, processing : {$ne:false}, crawlNext : {$lt:nowMinusX} },
                { "$set" : { "processing" : false } }
            );

            // Check there are no items processing
            processing = await Crawl
                .find({
                    domain : {$in:domains}, 
                    processing : true
                });
            if(processing.length > 0){
                log(`Crawl '${uuid}', Domain/s [${domainsJoined}], is busy processing ${processing.length}x items, aborting routine`);
                return resolve(0);
            }

            let schdGrp = []
            for(let i = 0; i < domains.length; i++){
                let qr = await Crawl
                            .find({
                                domain        : domains[i],
                                crawlNext     : {$lt:now},
                                processing    : {$ne:true} // false or undefined
                            })
                            .sort({crawlNext:'ascending'})
                            .limit(constants.CRAWL_BATCH_SIZE);
                schdGrp.push(qr)
            }

            let lenGrp = schdGrp.length;
            let lenMax = schdGrp.reduce((acc,cur)=>{ return Math.max(acc,cur.length)}, 0);
            let lenTot = schdGrp.reduce((acc,cur)=>{ return acc + cur.length }, 0);

            // Get number of items
            if(lenTot <= 0){
                log(`Crawl '${uuid}', Domain/s [${domainsJoined}], Up to Date`);
                return resolve(0);
            }
            
            // Mark as processing
            for(let i = 0; i < lenGrp; i++){
                for(let j = 0; j < schdGrp[i].length; j++){
                    try{
                        schdGrp[i][j].processing = true;
                        schdGrp[i][j] = await schdGrp[i][j].save();
                    }catch(err){
                        error(err);
                    }
                }
            }

            // Count the successes
            let success = 0;

            // If items to crawl, process each one ... 
            log(`Crawl '${uuid}', Domain/s [${domainsJoined}], Crawling ${lenGrp}x URLS`);

            // Store previous domain, to sleep if consecutive crawls
            let domainPrevious  = undefined;
            let consecutive     = 0;

            // Flag to abort if necessesary
            let abort           = false;
            let abortedIDs      = [];

            // Keep track of how many have been attempted
            let attempted       = 0;

            // Now iterate over the domains and urls for each domain
            for(let urlIX = 0; urlIX < lenMax; urlIX++){        //0 ... n-1 max urls per domain
                for(let domIX = 0; domIX < lenGrp; domIX++){    //0 ... k-1 domains
                    
                    // Increment Completed
                    attempted++;

                    //Ensure urls aren't out of bounds for domain
                    if(urlIX >= schdGrp[domIX].length) continue;
                    
                    // Now crawl for the url and domain index
                    try{

                        // Get the crawl object for the indices.
                        let crawl = schdGrp[domIX][urlIX];

                        // Mark abort if hash has changed;
                        if(!abort){
                            let hash = await regHash();
                            abort = Boolean(hash !== hashOriginal);
                            if(abort) log(`Crawl '${uuid}' domainsHash has changed (${hashOriginal} -> ${hash}), resetting ${lenTot - attempted}x urls.`);
                        }

                        // Abort
                        if(abort){
                            abortedIDs.push(ObjectId(crawl._id));
                            continue;
                        }

                        // Current Domain 
                        let domain  = crawl.domain

                        // Now random sleep if next domain is same
                        if(domainPrevious === domain){
                            consecutive++;
                            let randomInterval = getRandom(0,30) * 1000;
                            log(`Crawl '${uuid}', Domain ${domain} (${consecutive}x consecutive), sleeping for ${randomInterval}ms`);
                            await sleepAsync(randomInterval);
                        }else{
                            consecutive = 0;
                        }

                        // Update the previous domain record
                        domainPrevious = domain;

                        //Crawl
                        await crawl.crawl();

                        //Increment if success
                        success++;

                    }catch(err){
                        error(err);
                    }
                }
            }

            // Abort
            if(abortedIDs.length){
                try{
                    log(`>>>>> ABORTING ${abortedIDs.length}x CRAWLS <<<<<`);
                    await Crawl.updateMany(
                        { _id : {$in: abortedIDs}, processing : {$ne:false} },
                        { "$set" : { "processing" : false } }
                    );
                }catch(err){
                    error(`Problem aborting, ${err.message}`);
                }
            }

            // Done, Return the number of successes
            return resolve(success);   

        }catch(err){
            error(err);
            return resolve(0);
        }
    })
}

const crawlRoutine = async () => {
    let register, domains;
    let cpuCount = Math.max(os.cpus().length, 1);
    try{
        register = await CrawlRegister.findOne({uuid});
        if(!register){
            log(`Crawl '${uuid}' Not registered yet`);
            return 0;
        }
        // If register not ready abort this round...
        if(!register.ready){
            log(`Crawl '${uuid}' Register Not Ready`);
            return 0;
        }
        // Get the Domains to process, If the current domain list is empty abort this round ... 
        domains = register.domains;
        if(domains.length <= 0){
            log(`Crawl '${uuid}' Domain List Empty ABORTING`);
            return 0;
        }

        // Chunk the Domains, assign subset of the total domains to each cpu.
        let domainsChunked = chunk(domains, cpuCount);

        let now         = Date.now();
        let prom1       = domainsChunked.map(async (cnk, cpuId)=>{
            let cpu = `${cpuId + 1}/${cpuCount}`;
            await publishResult("CRAWL_DOMAINS_PARTITIONED", { crawlerId: uuid, cpu, domains: cnk });
            return crawlDomains(cnk)
        });
        let results     = await Promise.all(prom1);
        let total       = results.reduce((a, b) => a + b, 0);
        log(`Total items crawled: ${total}, in ${Date.now() - now}ms`);
        // await sleepAsync(getRandom(0,30000)); // sleep random time
        return total;
    }catch(err){
      error(err);
      return 0;
    }
};

(async ()=>{

    // Periodically Request URLs
    setInterval(async ()=>{
        try{
            let isMaster = await isMasterCrawler();
            if(isMaster)
                await publishResult(notifications.CRAWL_REQUEST_SEND_URLS);
        }catch(err){
            error(err);
        }
    },constants.CRAWL_REQUEST_SEND_URLS_INTERVAL)

    // Listen to Queues, and act accordingly
    listenToQueue(constants.SQS_TOPIC_REQUEST, async (data)=>{
        try{
            // Extract the message
            const parms = JSON.parse(data.Message);
            
            // Extract parameters
            let {url, force} = parms;

            // Validate url
            await validateURL(url);

            // Crawl Requested
            await publishResult(notifications.CRAWL_REQUEST_RECEIVED,{url});

            // Lookup
            let obj = await Crawl.findOne({url})

            // URL doesn't exist, create
            if(!obj){
                
                log(`Crawl Record Requested for: ${url}`)
                obj = await new Crawl({url});
                obj = await obj.save();

                // Report Created
                log(`Crawl Record Created: ${JSON.stringify(obj)}`);
                log(`Crawl '${obj._id}' next scheduled: ${obj.crawlNext}`);
            }

            // Force recrawl, set crawlNext date to now, and let it be picked up by next round
            if(force && obj){
                log(`Crawl '${obj._id}', Priority Crawl for: ${url}`);
                obj.crawlNext = Date.now();
                obj = await obj.save();
            }
        }catch(err){
            try{
                // Crawl Requested
                await publishResult(notifications.CRAWL_REQUEST_ERROR,{error:err})
            }catch(e){
                //handle
            }
            error(err);
        }
    });

})();

// Export the models
module.exports      = {
    crawlRoutine,
    crawlDomains
}
