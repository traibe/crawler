const URL                   = require("url").URL;
const {error}               = require('./log');
const util                  = require('util');
const fs                    = require('fs');
const { setIntervalAsync }  = require('set-interval-async/dynamic');

/*
  Get host url from full url.
*/
const getHost = (raw_url) => {
  return new URL(raw_url).hostname.replace(/^www\.+/g,"");
}

/*
  Get random numberbetween min and max
*/
const getRandom = (min, max) => {
  return Math.random() * (max - min) + min; 
}

/*
  Function to decode and then validate a URL
*/
const validateURL = (url) => {
  if(url == null || !url)
    throw("url must be provided");
  try {
    return new URL(url);
  } catch (err) {
    error(err.toString());
  }
  throw `Invalid url: '${url}'`;
};

// Set interval and run once immediately
// See: https://stackoverflow.com/a/7424165/1834057
const setIntervalAndRun = (fn, t) => {
  fn();
  return(setInterval(fn, t));
}

const setIntervalAndRunAsync = (fn, t) => {
  fn();
  return(setIntervalAsync(fn, t));
}
  
const chunk = (arr, numberGroups) => {
  if(numberGroups <= 1 || arr.length <= 1) return [arr];
  let size  = Math.ceil(arr.length / numberGroups);
  let res   = arr.reduce((acc, _, i) => {
    if (i % size === 0) acc.push(arr.slice(i, i + size))
    return acc
  }, [])
  while(res.length < numberGroups) 
    res.push([]);
  return res;
}

// https://stackoverflow.com/a/60407793/1834057
const arraysContainSameElements = (a1, a2) => {
  if(!Array.isArray(a1) || !Array.isArray(a2)) return false;
  return a1.length === a2.length && a1.every(a1item => a2.includes(a1item))
}

const sleepAsync = (time) => {
  return new Promise((resolve) => setTimeout(resolve, time))
}

// Promise version of fs.readFile, so can be used with async/await
const readFile = util.promisify(fs.readFile);

module.exports = {
    getHost,
    getRandom,
    validateURL,
    setIntervalAsync,
    setIntervalAndRun,
    setIntervalAndRunAsync,
    chunk,
    arraysContainSameElements,
    sleepAsync,
    readFile
}