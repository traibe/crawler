const cloudwatchMetrics = require('cloudwatch-metrics');
const os                = require('os');
const {
    Crawl,
    CrawlRegister, 
    isMasterCrawler
}                       = require('./modelsModified');

const {log, error}      = require('./log');

// Region
const region = process.env.AWS_REGION;
if(region){
    cloudwatchMetrics.initialize({region});
}

// Force if not master
const FORCE_SEND  = false;

// Variables
const NUMBER_CPUS               = Math.max(os.cpus().length, 1);
const NUMBER_DOMAINS_PER_CPU    = 1;
const DPDD_UPPER                = 25.0;
const DPDD_LOWER                =  5.0;

// Define the metrics group
const metricCrawlers = new cloudwatchMetrics.Metric('TRAIBE/MS', 'Count',[{
    Name  : "Service",
    Value : "Crawler" 
}]);

// Convenience Function
const put = (v,n) => {
    log(`${n}: ${v}`);
    metricCrawlers.put(v,n);
}

// Routine to send metrics
const sendMetrics = async () => {
    try{
        const master = await isMasterCrawler();
        if(FORCE_SEND || master){

            // Retrieve the Domains
            const domains = await Crawl
                .find({crawlNext : {$lte:new Date()}})
                .distinct('domain');
            
            // Number of Domains
            const nD = domains.length;
            try{
                put(nD, 'NumberDomain');
            }catch(err){
                error(err);
            }
            
            // Send Number of Pages to Crawl
            const nP   = await Crawl.countDocuments({crawlNext : {$lte : new Date()}}); 
            try{
                put(nP, 'NumberPagesRequireCrawl');
            }catch(err){
                error(err);
            }

            // Send Number of Pages to Crawl per Domain
            const dPdD = (Boolean(nD) && Boolean(nP)) ? nP/nD : 0;
            try{
                put(dPdD, 'NumberPagesRequireCrawlPerDomain');
            }catch(err){
                error(err);
            }

            // Send number of crawlers
            const nC   = await CrawlRegister.countDocuments({});
            try{
                put(nC, 'NumberCrawlersActive');
            }catch(err){
                error(err);
            }

            // Send alarm to scaleup/scaledown
            const   cMin    = 1,
                    cMax    = Math.ceil(nD / (NUMBER_CPUS * NUMBER_DOMAINS_PER_CPU));

            let dC = 0; //DEFAULT NO CHANGE
            if      (dPdD >= DPDD_UPPER && nC < cMax) dC = +1;  // NEED MORE
            else if (dPdD <= DPDD_LOWER && nC > cMin) dC = -1;  // NEED LESS
            else if (nC > cMax) dC = -1;                        // Too Many
            else if (nC < cMin) dC = +1;                        // Too Few
            try{
                put(dC, 'NumberCrawlersTargetDelta');
            }catch(err){
                error(err);
            }

        }
    }catch(err){
        error(err);
    }
}

module.exports = {
    sendMetrics
};