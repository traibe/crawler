const {log}             = require('./log');
const constants         = require('./constants');

// Load the schemas
let {
    mongoose,
    DatabaseConnection,
} = require('@traibe/models');

// Extract the credentials
const host                  = process.env.MONGO_HOST;
const user                  = process.env.MONGO_USER;
const password              = process.env.MONGO_PASSWORD;
const database              = process.env.MONGO_DATABASE;
const bucket_name           = process.env.BUCKET_NAME;

// Ensure env variables are set...
if(!host)           throw Error("MONGO_HOST is required as a environmental variable");
if(!user)           throw Error("MONGO_USER is required as a environmental variable");
if(!password)       throw Error("MONGO_PASSWORD is required as a environmental variable");
if(!database)       throw Error("MONGO_DATABASE is required as a environmental variable");
if(!bucket_name)    throw Error("BUCKET_NAME is required as a environmental variable");

// Make the connection
const db                    = new DatabaseConnection(host, user, password, database);

(()=>{
    log("Crawl Batch Size:", constants.CRAWL_BATCH_SIZE);
    log("Writing Archive to Bucket:",bucket_name);
})();

// Export the models
module.exports = {
    mongoose,
    db
};