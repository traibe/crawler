'use strict';

const url                   = require('url');
const AWS                   = require('aws-sdk'); // AWS SDK
const {log,error}           = require('./log');

const aws_access_key_id     = process.env.AWS_ACCESS_KEY_ID;
const aws_secret_access_key = process.env.AWS_SECRET_ACCESS_KEY;
const aws_region            = process.env.AWS_REGION;

if(!aws_access_key_id)
    throw new Error("AWS_ACCESS_KEY_ID is required as a environmental variable");
if(!aws_secret_access_key)
    throw new Error("AWS_SECRET_ACCESS_KEY is required as a environmental variable");
if(!aws_region)
    throw new Error("AWS_REGION is required as a environmental variable")

// Update the configuration
AWS.config.update(
    {
        accessKeyId: aws_access_key_id,
        secretAccessKey: aws_secret_access_key,
        region: aws_region 
    }
);

const awsS3 = async (params) => Promise.resolve(new AWS.S3(params));

/*
  Function to check if a proposed bucket name is OK.
*/
const isValidBucketName = async (bucketName) => {
    if(typeof(bucketName) != 'string')
        return Promise.reject("bucket name must be of type 'string'");
    const prefix = `Invalid Bucket '${bucketName || '<EMPTY_STRING>'}'`;
    if(bucketName.length <  3)
        return Promise.reject(prefix + ', must be length >= 3');
    if(bucketName.length > 63)
        return Promise.reject(prefix + ', must be length <= 63');
    if(bucketName.match(/[^a-z0-9\-\.]/))
        return Promise.reject(prefix + ', must contain only lower-case, numbers, - and .');
    if(bucketName.match(/^[^a-z]/))
        return Promise.reject(prefix + ', must start with a-z');
    if(bucketName.match(/\.\.+/))
        return Promise.reject(prefix + ', cannot contain consecutive .');
    if(bucketName.match(/\.\-|\-\.+/))
        return Promise.reject(prefix + ', cannot contain consecutive - and .');
    const oct = "(25[0-5]|2[0-4][0-9]|[01][0-9][0-9])" //0-255
    const fmt = RegExp(`(${oct}\\.){3}${oct}`);
    if(bucketName.match(fmt))
        return Promise.reject(prefix + ', cannot be part formatted as IP address');
    return Promise.resolve(bucketName);
}
  
  /*
    Function to check valid key name. Note this is more rigorous than
    what is actually the limit
  */
const isValidKey = async (key) => {
    if(typeof(key) != 'string')
        return Promise.reject('key argument is expected to be a string');
    const prefix = `Invalid Key '${key || '<EMPTY_STRING>'}'`;
    if(key.match(/^\//))
        return Promise.reject(prefix + ', not start with a /');
    if(key.match(/[^a-zA-Z0-9\-\.\/\_]/))
        return Promise.reject(prefix + ', must contain only alphanumeric, -._ and /');
    if(key.match(/\/\/+/))
        return Promise.reject(prefix + ', cannot contain consecutive \'//\'');
    return Promise.resolve(key)
}
  
const isValidACL = async(acl) => {
    const valid = ['private','public-read','public-read-write','aws-exec-read','authenticated-read','bucket-owner-read','bucket-owner-full-control','log-delivery-write'];
    if(typeof(acl) != 'string')
        return Promise.reject('ACL argument is expected to be a string');
    if(!valid.includes(acl))
        return Promise.reject(`ACL ('${acl}') expected to be one of: ['${valid.join("', '")}']`);
    return Promise.resolve(true)
}
   
const bucketExists = async (bucketName) => {
    const s3 = await awsS3();
    return new Promise((resolve, reject) => {
        if(typeof(bucketName) != 'string')
            return reject('bucketName argument is expected to be a string');
        s3.headBucket({
            Bucket: bucketName
        },(err) => { 
            if(err) error(`Bucket '${bucketName}' doesn't exist under the current profile`);
            return resolve(!Boolean(err));
        });
    })
}
  
const createBucket = async (bucketName) => {
    try{
        if(typeof(bucketName) != 'string')
            throw new Error('bucketName argument is expected to be a string');

        // Check that the bucketName name is OK!, will reject if not valid
        await isValidBucketName(bucketName);

        // Check the bucketName already exists
        if(await bucketExists(bucketName)) return Promise.resolve(true);
    
        // Get the AWS SDK Object
        const s3 = await awsS3();
    
        // Execute the creation
        return new Promise((resolve)=>{
            s3.createBucket({
                Bucket: bucketName,
            },(err) => {
                // Case 1: Bucket Created
                if (!err){
                    log(`Created new bucket: ${bucketName}.`)
                    return resolve(true);
    
                // Case 2: Bucket Exists, and We Own it.
                }else if(err.code === 'BucketAlreadyOwnedByYou'){
                    log(`Bucket '${bucketName}' exists, and is owned by you.`)
                    return resolve(true);

                // Case 3: Someone else owns it, or, invalid name
                }else if(err.code === 'BucketAlreadyExists'){
                    log(`Bucket '${bucketName}' exists, and is owned by someone else.`);
                    return resolve(false);

                // Case 4: Invalid name or other error
                }else{
                    error(`Problem creating '${bucketName}': ${err.toString()}`)
                    return resolve(false);
                }
            });
        })
        
    }catch(err){
        error(err);
        return Promise.resolve(false);
    }
}

const copyToNewKey = async (bucketName, key, newKey, acl='public-read') => {
    const timeStart = Date.now();
    try{
        log(`Copying within bucket '${bucketName}': ${key} -> ${newKey}`);
        await isValidACL(acl);
        log('is valid ACL');
        await isValidKey(newKey);
        log('Is valid Key');
        const s3 = await awsS3();
        log('GOT S3');
        return new Promise(async (resolve, reject)=>{

            log("Trying to copy object");
            log(bucketName);
            log(key);
            log(newKey);
            log(acl);

            s3.copyObject({ 
                ACL         : acl,
                CopySource  : encodeURI(`${bucketName}/${key}`),
                Bucket      : bucketName,
                Key         : encodeURI(newKey)
            },(err, data)=>{
                
                // Error
                if(err){
                    log(`Copy Fail: ${err}`)
                    return reject(err);
                }

                // Extract ETag from Copy Result
                let {ETag} = data.CopyObjectResult;

                // copyObject is missing the usual fields, so add
                let result = {
                    ETag       : ETag,
                    Location   : url.resolve(`https://${bucketName}.s3.amazonaws.com`,newKey),
                    key        : newKey,
                    Key        : newKey,
                    Bucket     : bucketName
                }
                
                // Report
                log(`Copy success: '${result.Location}', taking ${Date.now() - timeStart}ms.`);

                // Resolve
                resolve(result)
            })
        }); 
    }catch(err){
        return Promise.reject(err);
    }
}
  
/**
Store HTML to S3
*/
const saveToBucket = async (bucketName, key, body, acl='public-read') => {
    
    // Start time
    const timeStart = Date.now();

    try{

        // Check valid ACL
        await isValidACL(acl);

        // Check valid key
        await isValidKey(key);

        // Create Bucket if necessesary
        await createBucket(bucketName);

        // Get the s3 object
        const s3 = await awsS3({params: {Bucket: bucketName}});

        return new Promise((resolve, reject)=>{
            s3.upload({
                ACL: acl,
                Key: key,
                Body: body
            }, (err, data) => {
                if (err){
                    log(`Upload fail: '${err}'`);
                    return reject(err);
                }
                log(`Upload success: '${data.Location}', taking ${Date.now() - timeStart}ms.`);
                resolve(data)
            });
        })
        
    }catch(err){
        return Promise.reject(err);
    }
}
  

module.exports = {
    saveToBucket,
    createBucket,
    bucketExists,
    copyToNewKey
}