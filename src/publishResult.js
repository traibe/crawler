const {machineID:uuid}      = require('./machineID');
const constants             = require('./constants');   
const {TopicPublisher}      = require('@traibe/sns');
const topicPublisher        = new TopicPublisher(constants.SNS_TOPIC_RESULT);

/* Publish crawl result to news topic */
const publishResult = (stage, payload={}) => (
    topicPublisher.post({crawlerId:uuid, stage, timestamp: new Date(), ...payload})
);

module.exports = {
    publishResult
}