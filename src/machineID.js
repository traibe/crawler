const {v4:uuidv4}   = require('uuid');
const machineID     = uuidv4();
module.exports      = {
    machineID
}