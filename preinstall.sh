#!/bin/bash
#
#    ------------------------------------------------------------------------------
#    
#    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
#    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
#         888       888   .d88'     .8"888.      888   888     888  888         
#         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
#         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
#         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
#        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8    
#
#    ------------------------------------------------------------------------------
#    Bitbucket Dependencies
#    ------------------------------------------------------------------------------
#
#    Author: Nicholas Hamilton, PhD.
#    Date:   17th March 2021
#    Email:  nicholasehamilton@gmail.com
#

REPOS="models common sns sqs";
HOST="https://bitbucket.org/traibe";
BRANCH="master";

# Store the Current Directory
ROOT=${PWD}/node_modules ;

# Iterate over the repositories
for REPO in $REPOS
do
    # Variables
    ADDRESS=${HOST}/${REPO}/get/${BRANCH}.tar.gz ;
    OUTPUT=./${REPO}.tar.gz ;
    # Ensure node_modules folder exists
    mkdir -p $ROOT;
    # Step into current directory
    cd $ROOT && echo "Present Directory: ${PWD}";
    # Get the archive from the git repository
    echo "Retrieving ${ADDRESS} to ${OUTPUT}";
    wget -q $ADDRESS -O $OUTPUT ;
    # If successfully downloaded, proceed
    if [ -f ${OUTPUT} ]; then
        # Make Directory
        echo "Making Directory ${REPO}";
        mkdir -p $REPO;
        #Unpack
        echo "Unpacking ${OUTPUT}";
        tar -xzvf $OUTPUT --directory ./$REPO
        cd ./$REPO
        # Renaming the repo via wildcard to something predictable
        mv ./*-${REPO}-* ./.tempdir
        # Copy all files into root folder of the module
        cp -rf ./.tempdir/* ./ && rm -rf ./.tempdir
        cd ..
        # Install dependencies
        echo "Installing Dependencies for ${REPO}";
        cd ./$REPO
        npm install;
        cd ..
        # Cleanup
        echo "Removing ${OUTPUT}";
        rm -rf $OUTPUT ;
    else
        echo "File ${OUTPUT} doesn't exist"; 
    fi
done

echo "Done";
exit 0;
